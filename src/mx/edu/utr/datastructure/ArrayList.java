/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.edu.utr.datastructure;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 *
 * @author Victor
 */
public class ArrayList implements List{
     public Object[] elements;
    public int size;
    
    public ArrayList(){
        this(10);
    }
    
    public ArrayList(int inicialCapacity){
        elements = new Object[inicialCapacity];
    }
    
    
    
    @Override
    public boolean add(Object element){
        ensureCapacity(size + 1);
        elements[size++] = element;
        return true;
    }
     /**
     * Insert in the position that it is specified
     * @param index index at which the specified element is to be inserted.
     * @param element element to be inserted.
     */

    @Override
    public void add(int index, Object element) {
        rangeCheckForAdd(index);
         ensureCapacity(size + 1);
         elements[index] = element;
         size++;
    }
    
    /**
     * Removes all of the elements from this list. *
     */
    @Override
    public void clear() {
        for(int i = 0; i < size; i++){
            elements[i] = null;
            size = 0;
        }
        
    }

    /**
     * Returns the element at the specified position in this list.
     */
    @Override
    public Object get(int index) {
        if(index >size){
            throw new IndexOutOfBoundsException("Out Of Bound"); //To change body of generated methods, choose Tools | Templates.
        }
        return elements[index];
    }
    
    
    /**
     * Returns the index in this list of the first occurrence of the specified
     * element, or -1 if this list does not contain this element.
     */
    @Override
    public int indexOf(Object o) {
        if (o == null){
            for(int i=0; i<size; i++){
                if(o.equals(elements[i])){
                    return i;
                }
            }
        }else{
            for(int i=0; i<size; i++){
                if(o.equals(elements[i])){
                    return i;
                }
            }
        }
        return -1;
    }
    
     /**
     * Returns <tt>true</tt> if this list contains no elements.
     */
    @Override
    public boolean isEmpty() {
        return size == 0;
    }
    
    //To remove an element 
    /**
     * Removes the element at the specified position in this list.
     */
    @Override
    public Object remove(int index) {
        outOfBound(index);
        Object oldElement = elements[index];
        int numberMoved = size - index -1;
        if(numberMoved > 0){
            System.arraycopy(elements, index + 1, elements, index, numberMoved);
        }
        elements[--size] = null;
        return oldElement;
    }
    
    /**
     * Replaces the element at the specified position in this list with the
     * specified element.
     */

    @Override
    public Object set(int index, Object element) {
        if(index >size){
            throw new UnsupportedOperationException("Not supported yet.");
        } 
        Object old = elements[index];
        elements[index] = element;
        return old;
    }

    /**
     * Returns the number of elements in this list.
     */
    
    @Override
    public int size() {
        return size;
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    //this method is to ensure that there is enough space if it is not added
    private void ensureCapacity(int minCapacity) {
        int oldCapacity = elements.length;
        if(minCapacity>oldCapacity){
            int newCapacity = oldCapacity*2;
            if(newCapacity < minCapacity){
                newCapacity = minCapacity;
            }
            elements = Arrays.copyOf(elements, newCapacity);
        }
    }

    private void rangeCheckForAdd(int index) {
        if(index > size || index < 0){
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
        
    }
    
    //To see if the object was found
    private void outOfBound(int index) {
        if(index >= size){
            throw new IndexOutOfBoundsException("Out Of Bound");
        } 
    }
//Those are the abstarct methods from the class that is implemented
    @Override
    public boolean contains(Object o) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Iterator iterator() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object[] toArray() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object[] toArray(Object[] ts) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean remove(Object o) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean containsAll(Collection clctn) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean addAll(Collection clctn) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean addAll(int i, Collection clctn) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean removeAll(Collection clctn) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean retainAll(Collection clctn) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int lastIndexOf(Object o) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ListIterator listIterator() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ListIterator listIterator(int i) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List subList(int i, int i1) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
}
